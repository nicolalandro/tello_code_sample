# Coding with tello
* lang: python3.7
* os: Linux
* requirements on each folder:
  * requirements.txt, for the python requirements 
  * requirements.sh, for the system requirements (mplayer for video streaming)

## Easy Tello
* folder: easy_tello
* description: some simpe example, battery status and box fly

## Keyboard Controller
* folder: keyboard_controller
* description: see drone image and stats, and fly with keyboard

## Joystick Controller
* folder: joystick_controller
* description: contains some code and example with joystick
* run: joystick_and_video.py

## Controller from zero
* folder: controller_from_zero
* description: study about pygame ui and controller, tellopy info e video streaming with the aim to create a simple joypad controller



