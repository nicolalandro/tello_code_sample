import pygame.locals


def connect_joystick():
    pygame.joystick.init()
    js = pygame.joystick.Joystick(0)
    js.init()
    js_name = js.get_name()
    print('Joystick name: ' + js_name)


if __name__ == '__main__':
    pygame.init()

    connect_joystick()

    while True:
        for event in pygame.event.get():
            if event.type == pygame.locals.JOYBUTTONUP:
                print('JOYBUTTONUP, button:', event.button)
            elif event.type == pygame.locals.JOYHATMOTION:
                print('JOYHATMOTION, value:', event.value)
            elif event.type == pygame.locals.JOYAXISMOTION:
                print('JOYAXISMOTION, axis:', event.axis, ', value:', event.value)
