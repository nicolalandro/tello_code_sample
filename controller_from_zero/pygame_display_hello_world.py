import pygame


def create_window(w, h):
    pygame.display.init()
    screen = pygame.display.set_mode((w, h))

    pygame.font.init()
    font = pygame.font.SysFont("Liberation Serif", 30)
    return screen, font


def quit():
    pygame.quit()
    exit()


def draw_text(position, text, text_color):
    label = font.render(text, 1, text_color)
    screen.blit(label, position)


if __name__ == '__main__':
    pygame.init()

    screen, font = create_window(1280, 720)

    text_color = (255, 255, 255)
    position = (20, 20)
    text = "Python and Pygame are Fun!"
    draw_text(position, text, text_color)
    pygame.display.update()

    while True:
        for event in pygame.event.get():
            # click window x button
            if event.type == pygame.QUIT:
                quit()
