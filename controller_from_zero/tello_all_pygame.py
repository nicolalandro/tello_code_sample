import time

import pygame

from controller_from_zero.joystik_reader_hello_world import connect_joystick
from controller_from_zero.pygame_display_hello_world import create_window
from controller_from_zero.tello_foto_hello_world import HandleFileReceived
from controller_from_zero.tello_info_hello_world import create_and_connect_drone
from controller_from_zero.tello_info_pygame import FlightDataHandler
from controller_from_zero.tello_video_pygame import VideoFrameHandler


class USBGamepad:
    R1 = 7
    L1 = 6
    R2 = 5
    L2 = 4

    TRIANGLE = 0
    CROSS = 2
    CIRCLE = 3
    SQUARE = 1

    START = 9
    SELECT = 8

    UP = (0, 1)
    DOWN = (0, -1)
    LEFT = (-1, 0)
    RIGHT = (1, 0)

    LEFT_Y = 1
    LEFT_X = 0
    RIGHT_Y = 2
    RIGHT_X = 3


class HandleJoystickEvent:
    def __init__(self, gamepad, drone):
        self.gamepad = gamepad
        self.drone = drone
        self.speed = 50

    def __call__(self, event):
        if event.type == pygame.locals.JOYBUTTONUP:
            # print('JOYBUTTONUP, button:', event.button)
            if event.button == self.gamepad.R1:
                # print('take off')
                self.drone.takeoff()
            elif event.button == self.gamepad.L1:
                # print('land')
                self.drone.land()
            elif event.button == self.gamepad.R2:
                drone.take_picture()
            elif event.button == self.gamepad.L2:
                print('take video')
            elif event.button == self.gamepad.TRIANGLE:
                # print('forward')
                self.drone.forward(self.speed)
            elif event.button == self.gamepad.CROSS:
                # print('backward')
                self.drone.backward(self.speed)
            elif event.button == self.gamepad.CIRCLE:
                # print('left')
                self.drone.left(self.speed)
            elif event.button == self.gamepad.SQUARE:
                # print('right')
                self.drone.right(self.speed)
        elif event.type == pygame.locals.JOYHATMOTION:
            # print('JOYHATMOTION, value:', event.value)
            if event.value[0] < 0:
                self.drone.counter_clockwise(self.speed)
            if event.value[0] == 0:
                self.drone.clockwise(0)
            if event.value[0] > 0:
                self.drone.clockwise(self.speed)
            if event.value[1] < 0:
                self.drone.down(self.speed)
            if event.value[1] == 0:
                self.drone.up(0)
            if event.value[1] > 0:
                self.drone.up(self.speed)
        elif event.type == pygame.locals.JOYAXISMOTION:
            # print('JOYAXISMOTION, axis:', event.axis, ', value:', event.value)
            if event.axis == self.gamepad.LEFT_Y:
                throttle = - self.speed * event.value
                # print('throttle:', throttle)
                drone.set_throttle(throttle)
            if event.axis == self.gamepad.LEFT_X:
                yaw = self.speed * event.value
                # print('yaw:', yaw)
                drone.set_yaw(yaw)
            if event.axis == self.gamepad.RIGHT_Y:
                pitch = self.speed * event.value
                # print('pitch:', pitch)
                drone.set_pitch(pitch)
            if event.axis == self.gamepad.RIGHT_X:
                roll = self.speed * event.value
                # print('roll:', roll)
                drone.set_roll(roll)


if __name__ == '__main__':
    pygame.init()
    connect_joystick()

    screen, font = create_window(1280, 720)
    pygame.display.flip()
    drone = create_and_connect_drone()

    if 'window' in pygame.display.get_wm_info():
        wid = pygame.display.get_wm_info()['window']
    print("Tello video WID:", wid)

    drone.start_video()
    drone.subscribe(drone.EVENT_VIDEO_FRAME, VideoFrameHandler(wid))
    drone.subscribe(drone.EVENT_FLIGHT_DATA, FlightDataHandler(screen, font))
    drone.subscribe(drone.EVENT_FILE_RECEIVED, HandleFileReceived())

    try:
        drone.drone.set_video_mode(True)
    except:
        text = 'Cannot set video mode, connet to the tello wifi'
        text_color = (255, 255, 255)
        label = font.render(text.strip(), 1, text_color)
        screen.blit(label, (270, 380))
        pygame.display.update()

    handle_joystick_event = HandleJoystickEvent(USBGamepad, drone)
    while True:
        time.sleep(0.01)  # loop with pygame.event.get() is too mush tight w/o some sleep
        for event in pygame.event.get():
            # click window x button
            if event.type == pygame.QUIT:
                drone.quit()
                pygame.quit()
                exit()
            else:
                handle_joystick_event(event)
