import datetime
import os

from controller_from_zero.tello_info_hello_world import create_and_connect_drone


class HandleFileReceived:

    def __call__(self, event, sender, data):
        path = '%s/Pictures/tello-%s.jpeg' % (
            os.getenv('HOME'),
            datetime.datetime.now().strftime('%Y-%m-%d_%H%M%S'))
        with open(path, 'wb') as fd:
            fd.write(data)
        print('Saved photo to %s' % path)


if __name__ == '__main__':
    drone = create_and_connect_drone()

    drone.subscribe(drone.EVENT_FILE_RECEIVED, HandleFileReceived())

    drone.take_picture()

    try:
        while True:
            pass
    except:
        drone.quit()
        exit()
