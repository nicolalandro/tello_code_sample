import tellopy

ALL_DATA_DICTIONARY = ['ALT', 'SPD', 'BAT', 'WIFI', 'CAM', 'MODE']


def flight_data_handler(event, sender, data):
    text = str(data)
    data_dictionary = {d.split(': ')[0]: d.split(': ')[1] for d in text.split('|')}
    print(data_dictionary)


def create_and_connect_drone():
    drone = tellopy.Tello()
    drone.connect()
    # drone.wait_for_connection(5.0)
    return drone


if __name__ == '__main__':
    drone = create_and_connect_drone()

    drone.subscribe(drone.EVENT_FLIGHT_DATA, flight_data_handler)

    try:
        while True:
            pass
    except:
        drone.quit()
        exit()
