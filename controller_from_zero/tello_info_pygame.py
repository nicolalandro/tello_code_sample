import pygame
from pygame.rect import Rect

from controller_from_zero.pygame_display_hello_world import create_window
from controller_from_zero.tello_info_hello_world import create_and_connect_drone


class FlightDataHandler:
    def __init__(self, screen, font, text_color=(255, 255, 255), base_position=(20, 20)):
        self.x = base_position[0]
        self.y = base_position[1]
        self.text_color = text_color
        self.font = font
        self.screen = screen

    def __call__(self, event, sender, data):
        self.screen.fill((0, 0, 0))
        for i, text in enumerate(str(data).split('|')):
            label = self.font.render(text.strip(), 1, self.text_color)
            self.screen.blit(label, (self.x, self.y + (i * self.y + i * 20)))
        pygame.display.update(Rect(self.x, self.y, self.x + 120, self.y + 400))
        # pygame.display.update()


if __name__ == '__main__':
    screen, font = create_window(1280, 720)
    drone = create_and_connect_drone()

    drone.subscribe(drone.EVENT_FLIGHT_DATA, FlightDataHandler(screen, font))

    while True:
        for event in pygame.event.get():
            # click window x button
            if event.type == pygame.QUIT:
                drone.quit()
                pygame.quit()
                exit()
