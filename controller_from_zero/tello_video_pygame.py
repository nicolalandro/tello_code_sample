import time
from subprocess import Popen, PIPE
import pygame

from controller_from_zero.pygame_display_hello_world import create_window
from controller_from_zero.tello_info_hello_world import create_and_connect_drone


class VideoFrameHandler:
    def __init__(self, wid):
        self.cmd = ['mplayer', '-fps', '35', '-really-quiet', '-wid', str(wid)]
        self.video_player = Popen(self.cmd + ['-'], stdin=PIPE)
        self.frame_skip = 0

    def __call__(self, event, sender, data):
        try:
            if 0 < self.frame_skip:
                self.frame_skip = self.frame_skip - 1
            else:
                self.video_player.stdin.write(data)
        except IOError as err:
            print(str(err))
            self.video_player = Popen(self.cmd + ['-'], stdin=PIPE)

    def quit(self):
        self.frame_skip = 500
        self.video_player.kill()


if __name__ == '__main__':
    screen, font = create_window(1280, 720)
    pygame.display.flip()
    drone = create_and_connect_drone()

    if 'window' in pygame.display.get_wm_info():
        wid = pygame.display.get_wm_info()['window']
    print("Tello video WID:", wid)

    drone.start_video()
    drone.subscribe(drone.EVENT_VIDEO_FRAME, VideoFrameHandler(wid))

    try:
        drone.drone.set_video_mode(True)
    except:
        text = 'Cannot set video mode, connet to the tello wifi'
        text_color = (255, 255, 255)
        label = font.render(text.strip(), 1, text_color)
        screen.blit(label, (270, 380))
        pygame.display.update()

    while True:
        time.sleep(0.01)  # loop with pygame.event.get() is too mush tight w/o some sleep
        for event in pygame.event.get():
            # click window x button
            if event.type == pygame.QUIT:
                drone.quit()
                pygame.quit()
                exit()
