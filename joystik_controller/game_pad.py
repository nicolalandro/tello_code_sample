import os
import json
import pygame
from joystick_mapping import JsonJoystick


def get_buttons():
    but = None
    try:
        js = pygame.joystick.Joystick(0)
        js.init()
        js_name = js.get_name()
        print('Joystick name: ' + js_name)
        
        base_path = 'mappings'
        for j in os.listdir(base_path):
            file_path = os.path.join(base_path, j)
            c = JsonJoystick(file_path)
            if c.name == js_name:
                but = c
    except pygame.error:
        pass

    return but

def update(old, new, max_delta=0.3):
    if abs(old - new) <= max_delta:
        res = new
    else:
        res = 0.0
    return res

class HandleInputEvent:
    def __init__(self, buttons, flight_data_recording):
        self.speed = 0.0
        self.throttle = 0.0
        self.yaw = 0.0
        self.pitch = 0.0
        self.roll = 0.0
        self.buttons = buttons
        self.flight_data_recording = flight_data_recording

    def __call__(self, drone, e):
        # if "{'joy': 0, 'axis': 2, 'value': 0.0}" != str(e.__dict__):
        #     print(e.__dict__)
        if e.type == pygame.locals.JOYAXISMOTION:
            # ignore small input values (Deadzone)
            if -self.buttons.DEADZONE <= e.value and e.value <= self.buttons.DEADZONE:
                e.value = 0.0
            if e.axis == self.buttons.LEFT_Y:
                self.throttle = update(
                    self.throttle, e.value * self.buttons.LEFT_Y_REVERSE)
                drone.set_throttle(self.throttle)
            if e.axis == self.buttons.LEFT_X:
                self.yaw = update(self.yaw, e.value *
                                  self.buttons.LEFT_X_REVERSE)
                drone.set_yaw(self.yaw)
            if e.axis == self.buttons.RIGHT_Y:
                self.pitch = update(self.pitch, e.value *
                                    self.buttons.RIGHT_Y_REVERSE)
                drone.set_pitch(self.pitch)
            if e.axis == self.buttons.RIGHT_X:
                self.roll = update(self.roll, e.value *
                                   self.buttons.RIGHT_X_REVERSE)
                drone.set_roll(self.roll)
        elif e.type == pygame.locals.JOYHATMOTION:
            if e.value[0] < 0:
                drone.counter_clockwise(self.speed)
            if e.value[0] == 0:
                drone.clockwise(0)
            if e.value[0] > 0:
                drone.clockwise(self.speed)
            if e.value[1] < 0:
                drone.down(self.speed)
            if e.value[1] == 0:
                drone.up(0)
            if e.value[1] > 0:
                drone.up(self.speed)
        elif e.type == pygame.locals.JOYBUTTONDOWN:
            if e.button == self.buttons.LAND:
                drone.land()
            elif e.button == self.buttons.UP:
                drone.up(self.speed)
            elif e.button == self.buttons.DOWN:
                drone.down(self.speed)
            elif e.button == self.buttons.ROTATE_RIGHT:
                drone.clockwise(self.speed)
            elif e.button == self.buttons.ROTATE_LEFT:
                drone.counter_clockwise(self.speed)
            elif e.button == self.buttons.FORWARD:
                drone.forward(self.speed)
            elif e.button == self.buttons.BACKWARD:
                drone.backward(self.speed)
            elif e.button == self.buttons.RIGHT:
                drone.right(self.speed)
            elif e.button == self.buttons.LEFT:
                drone.left(self.speed)
        elif e.type == pygame.locals.JOYBUTTONUP:
            if e.button == self.buttons.TAKEOFF:
                if self.throttle != 0.0:
                    print('###')
                    print(
                        '### throttle != 0.0 (This may hinder the drone from taking off)')
                    print('###')
                drone.takeoff()
            elif e.button == self.buttons.UP:
                drone.up(0)
            elif e.button == self.buttons.DOWN:
                drone.down(0)
            elif e.button == self.buttons.ROTATE_RIGHT:
                drone.clockwise(0)
            elif e.button == self.buttons.ROTATE_LEFT:
                drone.counter_clockwise(0)
            elif e.button == self.buttons.FORWARD:
                drone.forward(0)
            elif e.button == self.buttons.BACKWARD:
                drone.backward(0)
            elif e.button == self.buttons.RIGHT:
                drone.right(0)
            elif e.button == self.buttons.LEFT:
                drone.left(0)
            elif e.button == self.buttons.TAKE_PICTURE:
                if self.speed == 0:
                    pass
                drone.take_picture()
            elif e.button == self.buttons.TAKE_VIDEO:
                print(self.flight_data_recording)
                self.flight_data_recording.toggle_recording(drone, self.speed)
