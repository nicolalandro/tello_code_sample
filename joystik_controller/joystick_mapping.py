import pygame
import pygame.locals
import json
import time

buttons = [
    # bumper triggers
    ('R1', 'TAKEOFF'),  # R1
    ('L1', 'LAND'),  # L1
    ('R2','TAKE_PICTURE'),  # R2
    ('L2', 'TAKE_VIDEO'),  # L2

    # buttons
    ('TRIANGLE','FORWARD'),  # TRIANGLE
    ('CROSS','BACKWARD'),  # CROSS
    ('SQUARE','LEFT'),  # SQUARE
    ('CIRCLE','RIGHT'),  # CIRCLE
]

dpad = [
    # d-pad
    ('d-pad UP', 'UP'),  # UP
    ('d-pad DOWN', 'DOWN'),  # DOWN
    ('d-pad LEFT', 'ROTATE_LEFT'),  # LEFT
    ('d-pad RIGHT', 'ROTATE_RIGHT'),  # RIGHT
]

analogs_axis = [
    ('left analog left', 'LEFT_X'),  # yaw
    ('left analog up', 'LEFT_Y'),  # throttle
    ('right analog left', 'RIGHT_X'),  # roll
    ('right analog up', 'RIGHT_Y'),  # pich
]

analogs = [
    # axis
    ('left analog left to right', 'LEFT_X_REVERSE'),
    ('left analog up to down', 'LEFT_Y_REVERSE'),
    ('right analog legt to right', 'RIGHT_X_REVERSE'),
    ('left analog up to down', 'RIGHT_Y_REVERSE'),
]

deadzoe = 'DEADZONE'   

class JsonJoystick:
    
    def __init__(self, json_path):
        with open(json_path, "r") as f: 
            json_read = f.read()
        mapping_dict = json.loads(json_read)
        print(mapping_dict)
        self.name = mapping_dict['name']
        
        self.TAKE_PICTURE = mapping_dict['TAKE_PICTURE']
        self.TAKE_VIDEO = mapping_dict['TAKE_VIDEO']
        
        # d-pad
        self.UP = mapping_dict['UP']
        self.DOWN = mapping_dict['DOWN']
        self.ROTATE_LEFT = mapping_dict['ROTATE_LEFT']
        self.ROTATE_RIGHT = mapping_dict['ROTATE_RIGHT']

        # bumper triggers
        self.TAKEOFF = mapping_dict['TAKEOFF']
        self.LAND = mapping_dict['LAND']
        # UNUSED = 5 #R2
        # UNUSED = 4 #L2

        # buttons
        self.FORWARD = mapping_dict['FORWARD']
        self.BACKWARD = mapping_dict['BACKWARD']
        self.LEFT = mapping_dict['LEFT']
        self.RIGHT = mapping_dict['RIGHT']

        # axis
        self.LEFT_X = mapping_dict['LEFT_X']
        self.LEFT_Y = mapping_dict['LEFT_Y']
        self.RIGHT_X = mapping_dict['RIGHT_X']
        self.RIGHT_Y = mapping_dict['RIGHT_Y']
        self.LEFT_X_REVERSE = mapping_dict['LEFT_X_REVERSE']
        self.LEFT_Y_REVERSE = mapping_dict['LEFT_Y_REVERSE']
        self.RIGHT_X_REVERSE = mapping_dict['RIGHT_X_REVERSE']
        self.RIGHT_Y_REVERSE = mapping_dict['RIGHT_Y_REVERSE']
        self.DEADZONE = mapping_dict['DEADZONE']



def get_button_id():
    button_id = None
    while button_id is None:
        for e in pygame.event.get():
            if e.type == pygame.locals.JOYBUTTONUP:
                button_id = e.button
    return button_id

def get_dpad_value():
    dpad_id = None
    while dpad_id is None:
        for e in pygame.event.get():
            if e.type == pygame.locals.JOYHATMOTION and e.value != (0, 0):
                for v in e.value:
                    if v != 0:
                        dpad_id = v
    return dpad_id

def get_analog_axis():
    axis_id = None
    axis_nums = []
    while axis_id is None:
        for e in pygame.event.get():
            if e.type == pygame.locals.JOYAXISMOTION:
                axis_id = e.axis
    time.sleep(1)
    for e in pygame.event.get():
        axis_nums.append(e.value)
    if sum(axis_nums) < 0:
        axis_num = -1.0
    else:
        axis_num = 1.0
    return axis_id, axis_num
    
def get_analog_reverse(axis):
    while True:
        for e in pygame.event.get():
            if e.type == pygame.locals.JOYAXISMOTION and e.axis == axis:
                print('correct axis', e.value)
            else:
                print(e.value)

def main():
    mapping_dictionary = dict()
    pygame.init()
    pygame.joystick.init()
    js = pygame.joystick.Joystick(0)
    js.init()
    js_name = js.get_name()
    print('Joystick name: ' + js_name)
    mapping_dictionary['name'] = js_name

    # read buttons
    for name, k_name in buttons:
        print('Press', name, '...')
        mapping_dictionary[k_name] = get_button_id()
        print('...', mapping_dictionary[k_name])
        # time.sleep(2)

        
    # read arrows
    for name, k_name in dpad:
        print('Press', name, '...')
        mapping_dictionary[k_name] = get_dpad_value()
        print('...', mapping_dictionary[k_name])
        # time.sleep(2)
    
    # analog axis
    for (name, k_name), (reverse_name, k_rev_name) in zip(analogs_axis, analogs) :
        print('Move', name, '...')
        mapping_dictionary[k_name], mapping_dictionary[k_rev_name] = get_analog_axis()
        print('...', mapping_dictionary[k_name], mapping_dictionary[k_rev_name])
    
    # deadzone
    mapping_dictionary[deadzoe] = 0.02
    
    # save json    
    json_mapping = json.dumps(mapping_dictionary)
    file_name = 'mappings/%s.json' % js_name.strip()
    with open(file_name, "w") as f:
        f.write(json_mapping)  
        
    # show generated json
    with open(file_name, "r") as f: 
        json_read = f.read()
    print('Generate json ad path', file_name, 'with value:')
    print(json_read)


if __name__ == '__main__':
    main()
