import time
import asyncio

import tellopy

import pygame
import pygame.display
import pygame.font

from ui_flight_handler import FlightDataHandler, FlightDataRecording, HandleFileReceived
from game_pad import get_buttons, HandleInputEvent


def main():
    pygame.init()
    pygame.display.init()
    pygame.display.set_mode((1280, 720))
    pygame.font.init()

    if 'window' in pygame.display.get_wm_info():
        wid = pygame.display.get_wm_info()['window']
    print("Tello video WID:", wid)

    # drone init
    drone = tellopy.Tello()
    drone.connect()
    # drone.wait_for_connection(5.0)
    drone.start_video()

    flight_data_recording = FlightDataRecording(wid)

    # drone callbacks
    # battery and other infos
    font = pygame.font.SysFont("dejavusansmono", 32)
    drone.subscribe(drone.EVENT_FLIGHT_DATA, FlightDataHandler(font, flight_data_recording))
    # image
    drone.subscribe(drone.EVENT_FILE_RECEIVED, HandleFileReceived())
    # video
    drone.subscribe(drone.EVENT_VIDEO_FRAME, flight_data_recording.video_frame_handler())

    # joystick
    buttons = get_buttons()
    if buttons is None:
        print('no supported joystick found')
        drone.quit()
        pygame.quit()
        exit(1)
    handle_input_evet = HandleInputEvent(buttons, flight_data_recording)

    # handle commands
    while True:
        for event in pygame.event.get():
            # click x button
            if event.type == pygame.QUIT:
                drone.quit()
                pygame.quit()
                exit()
            else:
                handle_input_evet(drone, event)


if __name__ == '__main__':
    main()
