from joystick_and_video import JoystickMY
from joystick_mapping import JsonJoystick


def test_with_old_class():
    json_mapping = JsonJoystick('./mappings/USB Gamepad.json')

    for s in JoystickMY.__dict__:
        if not s.startswith('_'):
            print('Command', s)
            old_val = getattr(JoystickMY, s)
            print('\tOld', old_val)
            new_val = getattr(json_mapping, s)
            print('\tNew', new_val)
            assert (old_val == new_val)


def test_with_json_classes(class_a, class_b):
    json_mapping1 = JsonJoystick(class_a)
    json_mapping2 = JsonJoystick(class_b)

    for s in JoystickMY.__dict__:
        if not s.startswith('_'):
            print('Command', s)
            old_val = getattr(json_mapping1, s)
            print('\tclass a', old_val)
            new_val = getattr(json_mapping2, s)
            print('\tclass b', new_val)
            # assert (old_val == new_val)


if __name__ == '__main__':
    path_a = './mappings/DragonRise.json'
    path_b = './mappings/DragonRise Inc.   Generic   USB  Joystick.json'
    test_with_json_classes(path_a, path_b)
