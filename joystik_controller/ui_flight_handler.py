import pygame
from subprocess import Popen, PIPE
import os
import datetime

def status_print(text):
        pygame.display.set_caption(text)
        
class HandleFileReceived:
    def __init__(self,):
        pass
    def __call__( self, event, sender, data):
        # Create a file in ~/Pictures/ to receive image data from the drone.
        path = '%s/Pictures/tello-%s.jpeg' % (
            os.getenv('HOME'),
            datetime.datetime.now().strftime('%Y-%m-%d_%H%M%S'))
        with open(path, 'wb') as fd:
            fd.write(data)
        status_print('Saved photo to %s' % path)

class FlightDataDisplay(object):
    # previous flight data value and surface to overlay
    _value = None
    _surface = None
    # function (drone, data) => new value
    # default is lambda drone,data: getattr(data, self._key)
    _update = None

    def __init__(self, key, format, font, colour=(255, 255, 255), update=None):
        self._key = key
        self._format = format
        self._colour = colour
        self.font = font

        if update:
            self._update = update
        else:
            self._update = lambda drone, data: getattr(data, self._key)

    def update(self, drone, data):
        new_value = self._update(drone, data)
        if self._value != new_value:
            self._value = new_value
            self._surface = self.font.render(
                self._format % (new_value,), True, self._colour)
        return self._surface


def flight_data_mode(drone, *args):
    return (drone.zoom and "VID" or "PIC")

class FlightDataRecording:
    def __init__(self, wid):
        self.video_recorder = None
        self.video_player = None
        self.date_fmt = None
        self.wid = wid
    
    def toggle_recording(self, drone, speed):
        if speed == 0:
            return

        if video_recorder:
            # already recording, so stop
            self.video_recorder.stdin.close()
            status_print('Video saved to %s' % video_recorder.video_filename)
            self.video_recorder = None
            return

        # start a new recording
        filename = '%s/Pictures/tello-%s.mp4' % (os.getenv('HOME'),
                                                datetime.datetime.now().strftime(self.date_fmt))
        self.video_recorder = Popen([
            'mencoder', '-', '-vc', 'x264', '-fps', '30', '-ovc', 'copy',
            '-of', 'lavf', '-lavfopts', 'format=mp4',
            # '-ffourcc', 'avc1',
            # '-really-quiet',
            '-o', filename,
        ], stdin=PIPE)
        self.video_recorder.video_filename = filename
        status_print('Recording video to %s' % filename)
        
    def video_frame_handler(self):
        def videoFrameHandler(event, sender, data):
            if self.video_player is None:
                cmd = [ 'mplayer', '-fps', '35', '-really-quiet' ]
                if self.wid is not None:
                    cmd = cmd + [ '-wid', str(self.wid) ]
                self.video_player = Popen(cmd + ['-'], stdin=PIPE)

            try:
                self.video_player.stdin.write(data)
            except IOError as err:
                status_print(str(err))
                self.video_player = None

            try:
                if self.video_recorder:
                    self.video_recorder.stdin.write(data)
            except IOError as err:
                status_print(str(err))
                video_recorder = None
        return videoFrameHandler
    
    def __call__(self, *args):
        return (self.video_recorder and "REC 00:00" or "")  # TODO: duration of recording

class FlightDataHandler:
    def __init__(self, font, flight_data_recording):
        self.prev_flight_data = None
        self.hud = [
            FlightDataDisplay('height', 'ALT %3d', font),
            FlightDataDisplay('ground_speed', 'SPD %3d', font),
            FlightDataDisplay('battery_percentage', 'BAT %3d%%', font),
            FlightDataDisplay('wifi_strength', 'NET %3d%%', font),
            FlightDataDisplay(None, 'CAM %s', font, update=flight_data_mode),
            FlightDataDisplay(None, '%s', font, colour=(255, 0, 0),
                              update=flight_data_recording),
        ]

    def _update_hud(self, hud, drone, flight_data):
        (w, h) = (158, 0)  # width available on side of screen in 4:3 mode
        blits = []
        for element in hud:
            surface = element.update(drone, flight_data)
            if surface is None:
                continue
            blits += [(surface, (0, h))]
            # w = max(w, surface.get_width())
            h += surface.get_height()
        h += 64  # add some padding
        overlay = pygame.Surface((w, h), pygame.SRCALPHA)
        overlay.fill((0, 0, 0))  # remove for mplayer overlay mode
        for blit in blits:
            overlay.blit(*blit)
        pygame.display.get_surface().blit(overlay, (0, 0))
        pygame.display.update(overlay.get_rect())

    def __call__(self, event, sender, data):
        global prev_flight_data
        text = str(data)
        if self.prev_flight_data != text:
            self._update_hud(self.hud, sender, data)
            self.prev_flight_data = text
