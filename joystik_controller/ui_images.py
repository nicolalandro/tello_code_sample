from subprocess import Popen, PIPE

class VideoFrameHandler:
    def __init__():
        self.video_player
        self.video_recorder
    
    def __call__(self, event, sender, data):
        if self.video_player is None:
            cmd = [ 'mplayer', '-fps', '35', '-really-quiet' ]
            if wid is not None:
                cmd = cmd + [ '-wid', str(wid) ]
            self.video_player = Popen(cmd + ['-'], stdin=PIPE)

        try:
            self.video_player.stdin.write(data)
        except IOError as err:
            status_print(str(err))
            self.video_player = None

        try:
            if self.video_recorder:
                self.video_recorder.stdin.write(data)
        except IOError as err:
            status_print(str(err))
            self.video_recorder = None